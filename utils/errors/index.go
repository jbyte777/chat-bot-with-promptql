package errorsutils

import (
	"fmt"
	"time"
)

func LogError(
	moduleName string,
	methodName string,
	err error,
) error {
	nowTimestamp := time.Now().Format("2006-01-01 00:00:00")

	return fmt.Errorf(
		"[%v] [%v / %v] => %v",
		nowTimestamp,
		moduleName,
		methodName,
		err.Error(),
	)
}
