package types

type TChatMessage struct {
	Id      uint   `json:"id" bson:"Id"`
	Author  string `json:"author" bson:"Author"`
	Message string `json:"message" bson:"Message"`
	Date    string `json:"date" bson:"Date"`
}

type TChatRawMessage struct {
	Action  string       `json:"action"`
	Payload TChatMessage `json:"payload"`
}
