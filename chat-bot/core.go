package chatbot

import (
	"fmt"

	types "gitlab.com/jbyte777/chat-bot-with-promptql/types"
	errorsutils "gitlab.com/jbyte777/chat-bot-with-promptql/utils/errors"
)

func (self *Chatbot) listenToMessages() {
	go func(self *Chatbot) {
		for msg := range self.inbox {
			self.tryHandleReplyMessageCommand(msg)
		}
	}(self)
}

func (self *Chatbot) clearInterpreterState() {
	self.interpreter.Instance.UnsafeExecute(`
		{~unsafe_clear_vars /}
		{~unsafe_clear_stack /}
	`)
}

func (self *Chatbot) initInterpreterState() {
	self.interpreter.Instance.UnsafeExecute(`
		{~unsafe_preinit_vars /}
		{~unsafe_clear_stack /}
	`)
}

func (self *Chatbot) execEmbeddingOnMessage(templName string, message *types.TChatMessage, callback TChatbotCallback) {
	interpreter := self.interpreter.Instance

	// expansion phase
	expansionRes := interpreter.Execute(
		fmt.Sprintf(
			`
				{~set to="exec_templ"}
					{~embed_exp name="template_%v"}
						{~data}author=%v{/data}
						{~data}message=%v{/data}
					{/embed_exp}
				{/set}
				{~embed_exp name="reply"}
					{~data}exec_template={~get from="exec_templ" /}{/data}
				{/embed_exp}
				`,
			templName,
			sanitizeText(message.Author),
			sanitizeText(message.Message),
		),
	)
	if expansionRes.Error != nil {
		self.clearInterpreterState()
		callback(
			errorsutils.LogError(
				"Chatbot",
				"GenerateResponseMessage",
				expansionRes.Error,
			),
			"",
		)
		return
	}
	resultOnExpStr, _ := expansionRes.ResultDataStr()
	errorOnExpStr, isErrorExpStr := expansionRes.ResultErrorStr()
	self.clearInterpreterState()
	if isErrorExpStr && len(errorOnExpStr) > 1 {
		callback(
			errorsutils.LogError(
				"Chatbot",
				"GenerateResponseMessage",
				fmt.Errorf("error on code expansion => %v", errorOnExpStr),
			),
			"",
		)
		return
	}

	// execution phase
	self.initInterpreterState()
	executionRes := interpreter.Execute(resultOnExpStr)
	if executionRes.Error != nil {
		self.clearInterpreterState()
		callback(
			errorsutils.LogError(
				"Chatbot",
				"GenerateResponseMessage",
				fmt.Errorf("error on code execution => %v", executionRes.Error),
			),
			"",
		)
		return
	}
	resultOnExecStr, _ := executionRes.ResultDataStr()
	errorOnExecStr, isErrorExecStr := executionRes.ResultErrorStr()
	self.clearInterpreterState()
	if isErrorExecStr && len(errorOnExecStr) > 1 {
		callback(
			errorsutils.LogError(
				"Chatbot",
				"GenerateResponseMessage",
				fmt.Errorf("error on code execution => %v", errorOnExecStr),
			),
			"",
		)
		return
	}

	// callback with result
	callback(nil, resultOnExecStr)
}
