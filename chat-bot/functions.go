package chatbot

import (
	"errors"
)

func escapeResultFunc(args []interface{}) interface{} {
	if len(args) < 1 {
		return errors.New("Not enough arguments for escape result")
	}

	arg, isArgStr := args[0].(string)
	if !isArgStr {
		return errors.New("Argument is not a string")
	}

	return sanitizeText(arg)
}
