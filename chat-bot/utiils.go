package chatbot

import (
	"math/rand"
	"time"
	"strings"
)

func generateNum() int {
	random := rand.New(rand.NewSource(time.Now().UnixMicro()))
	return random.Intn(10)
}

func isCharEscaped(ch rune) bool {
	return ch == '{' || ch == '}' ||
		ch == '<' || ch == '>' || ch == '%'
}

func sanitizeText(str string) string {
	res := strings.Builder{}
	strRune := []rune(str)

	for _, ch := range strRune {
		if isCharEscaped(ch) {
			res.WriteString("\\\\\\")
		}
		res.WriteRune(ch)
	}

	return res.String()
}
