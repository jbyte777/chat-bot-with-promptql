package chatbot

import (
	"errors"
	"fmt"
	"io/ioutil"
	"path"

	errorsutils "gitlab.com/jbyte777/chat-bot-with-promptql/utils/errors"
	promptqlcore "gitlab.com/jbyte777/prompt-ql/v5/core"
	promptql "gitlab.com/jbyte777/prompt-ql/v5/interpreter"
)

type Chatbot struct {
	interpreter         *promptql.PromptQL
	definitionsFilePath string
	inbox chan interface{}
}

func New() *Chatbot {
	interpreter := promptql.New(
		promptql.PromptQLOptions{
			OpenAiBaseUrl:               "http://localhost:8080",
			OpenAiListenQueryTimeoutSec: 240,
			PreinitializedInternalGlobals: promptqlcore.TGlobalVariablesTable{
				"_escape_result": escapeResultFunc,
			},
		},
	)
	definitionsPath := path.Join(
		"services",
		"chat-bot",
		"prompts.pql",
	)

	return &Chatbot{
		interpreter:         interpreter,
		definitionsFilePath: definitionsPath,
		inbox: make(chan interface{}, 100),
	}
}

func (self *Chatbot) Init() error {
	prompts, err := ioutil.ReadFile(self.definitionsFilePath)
	if err != nil {
		return errorsutils.LogError(
			"Chatbot",
			"Init",
			err,
		)
	}
	result := self.interpreter.Instance.UnsafeExecute(
		fmt.Sprintf(
			`
			{~session_begin /}
			%v
			{~unsafe_clear_vars /}
			{~unsafe_clear_stack /}
			`,
			string(prompts),
		),
	)
	if result.Error != nil {
		return errorsutils.LogError(
			"Chatbot",
			"Init",
			result.Error,
		)
	}
	resultErr, _ := result.ResultErrorStr()
	if len(resultErr) > 0 {
		return errorsutils.LogError(
			"Chatbot",
			"Init",
			errors.New(resultErr),
		)
	}

	self.listenToMessages()
	return nil
}

func (self *Chatbot) ExecCommand(cmd interface{}) {
	self.inbox <- cmd
}
