package chatbot

import (
	types "gitlab.com/jbyte777/chat-bot-with-promptql/types"
)

type TChatbotCallback func (err error, result string)

type TChatbotReplyCommand struct {
	Message *types.TChatMessage
	Callback TChatbotCallback
}
