package chatbot

func (self *Chatbot) tryHandleReplyMessageCommand(cmd interface{}) {
	replyCommand, isReplyCommand := cmd.(TChatbotReplyCommand)
	if !isReplyCommand {
		return
	}

	choice := generateNum()
	if choice >= 5 && choice <= 7 {
		self.execEmbeddingOnMessage("question", replyCommand.Message, replyCommand.Callback)
	} else if choice == 8 {
		self.execEmbeddingOnMessage("joke", replyCommand.Message, replyCommand.Callback)
	}
}
